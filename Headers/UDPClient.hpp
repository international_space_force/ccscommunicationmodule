#ifndef UDPENCRYPTEDCLIENT_UDPCLIENT_HPP
#define UDPENCRYPTEDCLIENT_UDPCLIENT_HPP

#include <string>

#include <winsock2.h>
#include <stdio.h>
#include "Ws2tcpip.h"

#pragma comment(lib, "Ws2_32.lib")

class UDPClient {
private:
    int remoteServerPort;
    std::string remoteServerAddress;

    WSADATA wsaData;
    SOCKET socketC;
    struct sockaddr_in serverInfo;
    int len = 0;

    void InitializeConnection() {
        WSAStartup(MAKEWORD(2, 2), &wsaData);

        len = sizeof(serverInfo);
        serverInfo.sin_family = AF_INET;
        serverInfo.sin_port = htons(remoteServerPort);
        inet_pton(AF_INET, remoteServerAddress.c_str(), &serverInfo.sin_addr.s_addr);

        socketC = socket(AF_INET, SOCK_DGRAM, 0);
    }

public:

    UDPClient(int port, std::string address) {
        remoteServerPort = port;
        remoteServerAddress = address;

        InitializeConnection();
    }

    bool SentDatagramToServerSuccessfully(char* buffer, int bufferByteCount) {
        bool successfulSend = true;
        int errorCode = 0; // 0 is no error
        errorCode = sendto(socketC, buffer, bufferByteCount, 0, (sockaddr*)&serverInfo, len);
        if(errorCode == SOCKET_ERROR) {
            successfulSend = false;
            printf("UDP Client Send Error on %d\n", remoteServerPort);
        }
        return successfulSend;
    }

    void ReceiveDatagramFromServer(int bufferLimit, char* buffer, int& receivedByteCount) {
        int errorCode = 0; // 0 is no error
        errorCode = recvfrom(socketC, buffer, bufferLimit, 0, (sockaddr*)&serverInfo, &len);
        if(errorCode == SOCKET_ERROR) {
            printf("UDP Client Receive Error on %d\n", remoteServerPort);
        }
    }

};

#endif //UDPENCRYPTEDCLIENT_UDPCLIENT_HPP
