#include <cstdio>
#include <vector>

#include "Headers/UDPServer.hpp"
#include "Headers/ConcurrentQueue.hpp"
#include "Headers/UDPClient.hpp"
#include "Headers/ModuleDesignations.hpp"
#include "MAVLink/common/mavlink.h"
#include "Headers/ConcurrentFileLogger.hpp"

#include "Windows.h"

const int CCS_PASSTHROUGH_IP_INDEX = 1;
const int HEARTBEAT_MSG_SEND_RATE_INDEX = 1;
const int ROVER_PASSTHROUGH_IP_INDEX = 2;
const int HEARTBEAT_STATUS_INTERVAL_IN_MS = 10000;
const int DATA_LIMIT_SIZE = 3000;
const std::string KEY = "InternationalSpaceForce";
const std::string LOOPBACK_ADDRESS = "127.0.0.1";

ConcurrentFileLogger logger("CCSComModLog.txt");

std::string MAVLinkEnumToString(MAVLinkMsgModule moduleID) {
    std::string result = "EMPTY";
    switch(moduleID) {
        case UltrasonicSensor:
            result = "Ultrasonic Sensor";
            break;
        case PathPlanning:
            result = "Path Planning";
            break;
        case MotorControl:
            result = "Motor Control";
            break;
        case SoldierRetrieval:
            result = "Soldier Retrieval";
            break;
        case RoverCommunication:
            result = "Rover Com";
            break;
        case CentralExecutive:
            result = "Central Executive";
            break;
        case IEDSensor:
            result = "IED Sensor";
            break;
        case CameraControl:
            result = "Camera Control";
            break;
        case GUI:
            result = "GUI";
            break;
        case CCSCommunication:
            result = "CCS Com";
            break;
        case MissionExecutive:
            result = "Mission Executive";
            break;
        default:
            result = "ERROR";
            break;
    }
    return result;
}

std::stringstream MAVLinkMsgToStringStream(mavlink_message_t msg, bool isIncomingData) {
    std::stringstream result;
    if(isIncomingData) {
        result << "Incoming ";
    } else {
        result << "Outgoing ";
    }
    result << "MAVLinkMsg " << msg.msgid << ": " << MAVLinkEnumToString((MAVLinkMsgModule)msg.sysid);
    result << " --> " << MAVLinkEnumToString((MAVLinkMsgModule)msg.compid);
    result << " "<< msg.checksum << '\n';

    if(msg.msgid == MAVLINK_MSG_ID_COMMAND_INT) {
        mavlink_command_int_t mavCmdInt;
        mavlink_msg_command_int_decode(&msg, &mavCmdInt);
        result << "CmdID: " << mavCmdInt.command << std::endl;
        switch (mavCmdInt.command) {
            case MAV_CMD_DO_SET_MODE:
                if(mavCmdInt.param1 == MAV_MODE_MANUAL_ARMED) {
                    result << "ManMode: On" << std::endl;
                } else {
                    result << "ManMode: Off" << std::endl;
                }
                break;

            default:
                break;
        }
    }

    return result;
}

std::vector<char> EncryptOrDecryptData(std::vector<char> rawData) {
    std::vector<char> encryptedData({});

    for(int index = 0; index < rawData.size(); index++) {
        if (rawData[index] != '\n')
            encryptedData.push_back((char)rawData[index] ^ KEY[index % KEY.size()]);
        else
            encryptedData.push_back(rawData[index]);
    }

    return encryptedData;
}

std::vector<char> CharArrayToVector(char* inputData, int inputDataSize) {
    std::vector<char> result(inputData, inputData + inputDataSize);
    return result;
}

void VectorToCharArray(std::vector<char> inputData, char* outputData) {
    if(inputData.size() <= DATA_LIMIT_SIZE) {
        std::copy(inputData.begin(), inputData.end(), outputData);
    }
}

[[noreturn]] void ParseMessagesInQueue(ConcurrentQueue<std::vector<char>>& messagesToParse,
                                       ConcurrentQueue<std::vector<char>>& messagesToGuiModule,
                                       ConcurrentQueue<std::vector<char>>& messagesToMissionExecutiveModule,
                                       ConcurrentQueue<std::vector<char>>& outGoingExternalInterfaceMsgs) {
    while(true) {
        char receivedBuffer[DATA_LIMIT_SIZE];
        int receivedByteCount = 0;
        std::vector<char> dataToBeSent;

        messagesToParse.pop(dataToBeSent);
        receivedByteCount = dataToBeSent.size();
        VectorToCharArray(dataToBeSent, receivedBuffer);

        mavlink_message_t msg;
        mavlink_status_t status;
        mavlink_heartbeat_t heartbeat;
        unsigned int firstCharInBuffer = -1;
        bool dataWasCorrectlyParsed = false;

        for (int bufferIndex = 0; bufferIndex < receivedByteCount; ++bufferIndex) {
            firstCharInBuffer = dataToBeSent[bufferIndex];
            //printf("%02x ", (unsigned char) firstCharInBuffer);

            if (mavlink_parse_char(MAVLINK_COMM_0, dataToBeSent[bufferIndex], &msg, &status)) {
                dataWasCorrectlyParsed = true;
                logger.WriteToFile(MAVLinkMsgToStringStream(msg, true));
                switch (msg.compid) {
                    case GUI:
                        messagesToGuiModule.push(dataToBeSent);
                        break;

                    case MissionExecutive:
                        messagesToMissionExecutiveModule.push(dataToBeSent);
                        break;

                    case UltrasonicSensor:
                    case IEDSensor:
                    case PathPlanning:
                    case MotorControl:
                    case SoldierRetrieval:
                    case CameraControl:
                    case CentralExecutive:
                        // Outgoing External Interface to Rover
                        outGoingExternalInterfaceMsgs.push(dataToBeSent);
                        break;

                    case CCSCommunication:
                        // Received heartbeat status from mission exec
                        break;

                    default:
                        logger.WriteToFile("Invalid MAVLink Data Received\n");
                        break;
                }
            }
        }
        if(!dataWasCorrectlyParsed) {
            logger.WriteToFile("Invalid Data Received\n");
        }
    }
}

[[noreturn]] void WaitForDataToBeReceived(UDPServer& serverConnection,
        ConcurrentQueue<std::vector<char>>& dataReceived) {
    while(true) {
        char receivedBuffer[DATA_LIMIT_SIZE];
        int receivedByteCount = 0;

        //printf("Waiting for local data: \n");
        serverConnection.GetDatagram(std::ref(receivedByteCount), receivedBuffer, DATA_LIMIT_SIZE);
        //printf("Received local data: %s\n", receivedBuffer);

        dataReceived.push(CharArrayToVector(receivedBuffer, receivedByteCount));
    }
}

[[noreturn]] void SendDataFromQueue(UDPClient& clientModuleConnection, ConcurrentQueue<std::vector<char>>& queue) {
    while(true) {
        char receivedBuffer[DATA_LIMIT_SIZE];
        int receivedByteCount = 0;
        std::vector<char> dataToBeSent;

        queue.pop(dataToBeSent);
        receivedByteCount = dataToBeSent.size();

        VectorToCharArray(dataToBeSent, receivedBuffer);

        //printf("Sending Data\n");
        clientModuleConnection.SentDatagramToServerSuccessfully(receivedBuffer, receivedByteCount);
    }
}

[[noreturn]] void SendDataToRover(UDPClient& roverConnection, ConcurrentQueue<std::vector<char>>& dataToSend) {
    while(true) {
        char receivedBuffer[DATA_LIMIT_SIZE];
        int receivedByteCount = 0;
        std::vector<char> dataToBeSent;

        dataToSend.pop(dataToBeSent);
        receivedByteCount = dataToBeSent.size();

        // Encrypt data to send over to Rover
        std::vector<char> encryptedData = EncryptOrDecryptData(dataToBeSent);
        VectorToCharArray(encryptedData, receivedBuffer);

        //printf("Sending Data\n");
        roverConnection.SentDatagramToServerSuccessfully(receivedBuffer, receivedByteCount);
    }
}

[[noreturn]] void ReceiveDataFromRover(UDPServer& roverConnection, ConcurrentQueue<std::vector<char>>& incomingData) {
    while(true) {
        char receivedBuffer[DATA_LIMIT_SIZE];
        int receivedByteCount = 0;

        //("Waiting for Rover data: \n");
        roverConnection.GetDatagram(std::ref(receivedByteCount), receivedBuffer, DATA_LIMIT_SIZE);

        // Need to decrypt for raw data from Rover
        std::vector<char> decryptedData = EncryptOrDecryptData(CharArrayToVector(receivedBuffer, receivedByteCount));
        incomingData.push(decryptedData);
    }
}

[[noreturn]] void SendHeartbeatStatusMessage(ConcurrentQueue<std::vector<char>>& dataQueue, int sendRateInSeconds) {
    while(true) {
        /* Send Heartbeat */
        uint8_t buf[DATA_LIMIT_SIZE];
        mavlink_message_t msg;
        uint16_t len;
        mavlink_msg_heartbeat_pack(CCSCommunication, MissionExecutive, &msg,
                MAV_TYPE_GROUND_ROVER, MAV_AUTOPILOT_GENERIC, MAV_MODE_GUIDED_ARMED,
                0, MAV_STATE_ACTIVE);
        len = mavlink_msg_to_send_buffer(buf, &msg);

        std::vector<char> dataToSend = CharArrayToVector((char*)buf, len);
        dataQueue.push(dataToSend);
        logger.WriteToFile(MAVLinkMsgToStringStream(msg, false));

        // Sleep for X milliseconds
        Sleep(HEARTBEAT_STATUS_INTERVAL_IN_MS);
    }
}

int main(int argc, char *argv[]) {
    int heartbeatSendRateInSecs = 10;
    std::string passthroughIP = "192.168.1.14";
    int passthroughListeningPort = CCSPassthroughModule;

    if(argc > 1) {
        heartbeatSendRateInSecs = std::stoi(argv[HEARTBEAT_MSG_SEND_RATE_INDEX]);
        passthroughIP = argv[ROVER_PASSTHROUGH_IP_INDEX];
        passthroughListeningPort = EncryptedFromCCSData;
    }

    // Concurrent queues used for data transmission
    // External to CCS interfaces
    ConcurrentQueue<std::vector<char>> dataToSendToRover;
    // Statically assigned
    UDPClient sendDataToRover(passthroughListeningPort, passthroughIP);
    // Statically assigned
    UDPServer receiveDataFromRover(EncryptedFromRoverData, "192.168.1.11");

    // Internal to CCS interfaces
    ConcurrentQueue<std::vector<char>> incomingDataToParse;
    ConcurrentQueue<std::vector<char>> dataToSendToGuiModule;
    ConcurrentQueue<std::vector<char>> dataToSendToMissionExecutiveModule;

    // Receivers and senders for data transmission
    UDPServer thisServer(CCSCommunicationModule, LOOPBACK_ADDRESS);
    UDPClient connectionToGuiModule(GUIModule, LOOPBACK_ADDRESS);
    UDPClient connectionToMissionExecutiveModule(MissionExecutiveModule, LOOPBACK_ADDRESS);

    // External interface connection
    std::thread sendToRover(SendDataToRover, std::ref(sendDataToRover), std::ref(dataToSendToRover));
    std::thread receiveFromRover(ReceiveDataFromRover, std::ref(receiveDataFromRover), std::ref(incomingDataToParse));

    // Main server functions for incoming data
    std::thread receiveData(WaitForDataToBeReceived, std::ref(thisServer), std::ref(incomingDataToParse));
    std::thread parseDataReceived(ParseMessagesInQueue, std::ref(incomingDataToParse),
                                  std::ref(dataToSendToGuiModule), std::ref(dataToSendToMissionExecutiveModule),
                                  std::ref(dataToSendToRover));

    // Main server functions for outgoing data
    std::thread sendHeartbeatMsgToMissionExecMod(SendHeartbeatStatusMessage,
                                                 std::ref(incomingDataToParse),
                                                 heartbeatSendRateInSecs);
    std::thread sendDataToGuiModule(SendDataFromQueue, std::ref(connectionToGuiModule),
            std::ref(dataToSendToGuiModule));
    std::thread sendDataToMissionExecutiveModule(SendDataFromQueue, std::ref(connectionToMissionExecutiveModule),
            std::ref(dataToSendToMissionExecutiveModule));

    printf("CCS Communication Module Executing\n");
    printf("Connecting Channel Link\n");
    printf("Using Encrypted Channel Link\n");
    logger.WriteToFile("Connecting Channel Link");
    logger.WriteToFile("Using Encrypted Channel Link");

    // Wait for threads to never finish
    receiveFromRover.join();
    sendToRover.join();
    receiveData.join();
    parseDataReceived.join();
    sendHeartbeatMsgToMissionExecMod.join();
    sendDataToGuiModule.join();
    sendDataToMissionExecutiveModule.join();

    return 0;
}